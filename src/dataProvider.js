import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:3001/graphql",
});

export default {
  products: () =>
    api
      .post("", {
        query: `query {
      products(limit: 4, page: 1, search: "un") {
        data { 
          id
          name
          price 
          sizes {
            size
            quantity
          }
        }
        count
      }
    }`,
      })
      .then((data) => data),
};
