import * as React from "react";
import { useQuery, Loading, Error } from "react-admin";

export const ProductList = (props) => {
  const { data, loading, error } = useQuery({
    type: "products",
  });

  if (loading) return <Loading />;
  if (error) return <Error />;
  if (!data) return null;

  return data.data.products.data.map((item) => (
    <ul>
      <li>{item.id}</li>
      <li>{item.name}</li>
    </ul>
  ));
};
