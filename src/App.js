import React from "react";
import { Admin, Resource } from "react-admin";
import { ProductList } from "./ProductList";
import dataProvider from "./dataProvider";

const App = () => {
  return (
    <Admin dataProvider={dataProvider}>
      <Resource name="products" list={ProductList} />
    </Admin>
  );
};

export default App;
